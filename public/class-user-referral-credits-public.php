<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://echo5digital.com
 * @since      1.0.0
 *
 * @package    User_Referral_Credits
 * @subpackage User_Referral_Credits/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    User_Referral_Credits
 * @subpackage User_Referral_Credits/public
 * @author     Joshua Flowers <joshua@echo5digital.com>
 */
class User_Referral_Credits_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/user-referral-credits-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Set a referrer cookie if present in URL
	 *
	 * @return void
	 */
	public function set_referrer_cookie() {

		if ( isset( $_GET['ufc'] )) {
			setcookie( 'referrer_user_id', intval( $_GET['ufc'] ), strtotime( '+7 days' ) );
		}

	}

	/**
	 * Add credits after a first-time purchase is made if referrer is set
	 *
	 * @param int $order_id
	 * @return void
	 */
	public function add_credits_for_first_time_purchase( $order_id ) {

		if ( ! $order_id )
			return;

		$order_count = wc_get_customer_order_count( get_current_user_id() );
		if ( $order_count == 1 && isset( $_COOKIE['referrer_user_id'] ) && $_COOKIE['referrer_user_id'] != get_current_user_id() ) {
			mycred_add( 'ufc', intval( $_COOKIE['referrer_user_id'] ), 10, 'Referred a new user' );
		}

	}

	/**
	 * Deduct credits from subtotal if any exist
	 *
	 * @param WC_Cart $cart
	 * @return void
	 */
	public function reduce_subtotal_with_credits( WC_Cart $cart ) {

		$balance = mycred_get_users_cred( get_current_user_id() );
		if ( $balance > 0 ){
			$credits = $cart->subtotal > $balance ? $balance : $cart->subtotal;
			$cart->add_fee( __( 'Referral credit', 'user-referral-credits' ), -$credits);
		}

	}

	/**
	 * Provide a discount to referred users
	 *
	 * @param WC_Cart $cart
	 * @return void
	 */
	public function discount_first_time_referred_order( WC_Cart $cart ) {

		$order_count = wc_get_customer_order_count( get_current_user_id() );
		if ( $order_count == 0 && isset( $_COOKIE['referrer_user_id'] ) && $_COOKIE['referrer_user_id'] != get_current_user_id() ){
			$credits = 10;
			$cart->add_fee( __( 'Referral credit', 'user-referral-credits' ), -$credits);
		}

	}

	/**
	 * Reduce user's credits after checkout since they were
	 * presumably used if they existed
	 *
	 * @param int $order_id
	 * @return void
	 */
	public function reduce_credits_after_checkout( $order_id ) {

		$balance = mycred_get_users_cred( get_current_user_id() );
		if ( $balance > 0 ){
			$order = wc_get_order( $order_id );
			$subtotal = $order->get_subtotal();
			$credits = $subtotal > $balance ? $balance : $subtotal;
			mycred_subtract( 'ufc', get_current_user_id(), $credits, 'Customer used credits' );
		}

	}

	/**
	 * Shortcode for referral links
	 *
	 * @param [type] $atts
	 * @return void
	 */
	public function referral_links( $atts ) {
		$output = '';
		$user_id = get_current_user_id();
		global $current_user;
		if ( $user_id != 0 ) {
			$referral_url = site_url( '/?ufc=' . $user_id );
			$subject = sprintf( __( 'You’ve been referred to %s', 'user-referral-credits' ), get_bloginfo( 'name' ) );
			$text_content = sprintf( __( '%s has referred you to The Meat Club! Enjoy $10 off your first purchase using this unique link to shop (the credit will apply at the point of checkout): %s', 'user-referral-credits' ), get_bloginfo( 'name'), array( $current_user->display_name, $referral_url ) );
			$content = sprintf( __( "Hi there!\n\nIf you click my unique referral link, you'll enjoy get $10 off your first order with %s (new members only).\n\nThe $10 credit will apply to your cart at the time of checkout and is valid for 7 days.\n\nHappy shopping!\n\n%s", 'user-referral-credits' ), get_bloginfo( 'name'), $referral_url );
			ob_start();
			$subject = str_replace( '+', '%20', urlencode( $subject ) );
			$content = str_replace( '+', '%20', urlencode( $content ) );
			?>
				<div class="ufc-referrer-share-links">
					<span class="ufc-description">
						<?php _e( 'Enjoy $10 off, for you and your friends, when you share The Meat Club love', 'user-referral-credits'); ?>
					</span>
					<!--email_off-->
					<a class="ufc-refer-email" href="mailto:?subject=<?php echo $subject; ?>&amp;body=<?php echo $content; ?>"><i class="fa fa-envelope"></i> <?php _e( 'Email', 'user-referral-credits'); ?></a>
					<!--/email_off-->
					<a class="ufc-refer-whatsapp" href="whatsapp://send?text=<?php echo $content; ?>">
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="90px" height="90px" viewBox="0 0 90 90" style="enable-background:new 0 0 90 90;" xml:space="preserve"><script xmlns=""/>
							<g>
								<path id="WhatsApp" d="M90,43.841c0,24.213-19.779,43.841-44.182,43.841c-7.747,0-15.025-1.98-21.357-5.455L0,90l7.975-23.522   c-4.023-6.606-6.34-14.354-6.34-22.637C1.635,19.628,21.416,0,45.818,0C70.223,0,90,19.628,90,43.841z M45.818,6.982   c-20.484,0-37.146,16.535-37.146,36.859c0,8.065,2.629,15.534,7.076,21.61L11.107,79.14l14.275-4.537   c5.865,3.851,12.891,6.097,20.437,6.097c20.481,0,37.146-16.533,37.146-36.857S66.301,6.982,45.818,6.982z M68.129,53.938   c-0.273-0.447-0.994-0.717-2.076-1.254c-1.084-0.537-6.41-3.138-7.4-3.495c-0.993-0.358-1.717-0.538-2.438,0.537   c-0.721,1.076-2.797,3.495-3.43,4.212c-0.632,0.719-1.263,0.809-2.347,0.271c-1.082-0.537-4.571-1.673-8.708-5.333   c-3.219-2.848-5.393-6.364-6.025-7.441c-0.631-1.075-0.066-1.656,0.475-2.191c0.488-0.482,1.084-1.255,1.625-1.882   c0.543-0.628,0.723-1.075,1.082-1.793c0.363-0.717,0.182-1.344-0.09-1.883c-0.27-0.537-2.438-5.825-3.34-7.977   c-0.902-2.15-1.803-1.792-2.436-1.792c-0.631,0-1.354-0.09-2.076-0.09c-0.722,0-1.896,0.269-2.889,1.344   c-0.992,1.076-3.789,3.676-3.789,8.963c0,5.288,3.879,10.397,4.422,11.113c0.541,0.716,7.49,11.92,18.5,16.223   C58.2,65.771,58.2,64.336,60.186,64.156c1.984-0.179,6.406-2.599,7.312-5.107C68.398,56.537,68.398,54.386,68.129,53.938z"/>
							</g>
						</svg>
						<?php _e( 'WhatsApp', 'user-referral-credits'); ?>
					</a>
					<a class="ufc-refer-sms" href="sms:?&body=<?php echo $content; ?>"><i class="fa fa-mobile"></i> <?php _e( 'SMS', 'user-referral-credits'); ?></a>
				</div>
			<?php
			$output = ob_get_clean();
		}
		return $output;
	}

	/**
	 * Display referral links in toolbar
	 *
	 * @return void
	 */
	public function display_referral_links() {
		echo do_shortcode( '[referral_links]' );
	}

}
