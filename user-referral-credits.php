<?php

/**
 * The plugin bootstrap file
 *
 * @link              https://echo5digital.com
 * @since             1.0.0
 * @package           User_Referral_Credits
 *
 * @wordpress-plugin
 * Plugin Name:       User Referral Credits
 * Plugin URI:        #
 * Description:       Create referral links and credits for referred users.
 * Version:           1.0.0
 * Author:            Joshua Flowers
 * Author URI:        https://echo5digital.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       user-referral-credits
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'USER_REFERRAL_CREDITS', '1.0.2' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-user-referral-credits.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_user_referral_credits() {

	$plugin = new User_Referral_Credits();
	$plugin->run();

}
run_user_referral_credits();
